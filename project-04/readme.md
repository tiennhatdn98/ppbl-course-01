# PPBL Course #1 - Capstone Project: Escrow
### 31 March - 22 April 2022

## Official Documentation:
- [Plutus V1 Ledger Contexts](https://playground.plutus.iohkdev.io/doc/haddock/plutus-ledger-api/html/Plutus-V1-Ledger-Contexts.html)
- [Plutus V1 Ledger Value](https://playground.plutus.iohkdev.io/doc/haddock/plutus-ledger-api/html/Plutus-V1-Ledger-Value.html)

## Next Steps:
- Phase 1: Build a working escrow contract in Plutus, and accompanying front-end interfaces, that are applicable for our initial use cases (to be linked here as git repos are initialized). In Phase 1, our focus is on *working code*, not yet on performance, efficiency, or scalability.
- Phase 2: Starting from the working prototypes we build in Phase 1, improve performance and reduce the transactions costs of our Escrow Contract(s).
- Phase 3: Look ahead to the next iteration of Plutus Project-Based Learning.

## Repo Contents:
1. `ValidateWithToken.hs` - validate if a given auth token is input from and output to a Contract address
2. `ValidateWithTokenInOutput.hs` - validate if a given auth token is output to a Contract address (stays there)
3. `ValidateWithTokenInUserInput.hs` - validate if an incoming UTXO holds the auth token
4. NEXT: use Redeemer to establish different ways of interacting with Contract
5. NEXT: rather than parameter, hold auth token information in Datum, and unlock targeted UTXOs
6. NEXT: explore dynamic minting of authentication UTXOs

---

## 1: `ValidateWithToken.hs`
- This example is not really what we want, but it provides a place to start.

### 1a: Compile and build Contract Address
- This is a parameterized validator: must define the Auth Token

```
cd /project-04/output
cardano-cli address build --payment-script-file validate-with-token.plutus --testnet-magic 1097911063 --out-file validate-with-token.addr
```

### 1b: Lock Funds
- In `validate-with-token.plutus`, we expect Datum and Redeemer to be integers, but it doesn't matter which. (see `/project-04/src/PpblEscrow/ValidateWithToken.hs`)
- Just Hash a number, and note it:
```
cardano-cli transaction hash-script-data --script-data-value 1618
> 2da1c63e7646ce8cc514113c66e9cefb79e482210ad1dadb51c2a17ab14cf114
```
- In this case, we'll never be able to unlock a transaction that lacks the specified Auth Token (try it!), so we'll send a UTXO with at least 1 PlutusPBLCourse01 token.

#### Set Variables
```
SENDER
SENDERKEY
CONTRACT=addr_test1wpszl2g7u63xdj9tv2e6qr750f6k6eldctpl9a6h9ty5fycqjugpc"
DATUMHASH="2da1c63e7646ce8cc514113c66e9cefb79e482210ad1dadb51c2a17ab14cf114"
TXIN=
```

```
cardano-cli transaction build \
--alonzo-era \
--tx-in $SENDERTXIN \
--tx-out $CONTRACT+"2000000 + 2 3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c.506c7574757350424c436f757273653031" \
--tx-out-datum-hash $DATUMHASH \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file tx.raw \
--testnet-magic 1097911063

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file tx.raw \
--out-file tx.signed

cardano-cli transaction submit \
--tx-file tx.signed \
--testnet-magic 1097911063
```

### 1c: Unlock Funds
- Here, the *Contract's own* TXIN must include the specified auth token.
- `ValidateWithToken.hs` assumes that the Auth token is an input from and output to the Contract address
- This is not really what we want: we'd like a user to be able to hold the asset and use it to unlock a contract.

#### There are several ways we can test errors here


```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $SENDERTXIN \
--tx-in $CONTRACTTXIN \
--tx-in-script-file validate-with-token.plutus \
--tx-in-datum-value 1618 \
--tx-in-redeemer-value 0 \
--tx-in-collateral $COLLATERAL \
--tx-out $CONTRACT+"2000000 + 2 3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c.506c7574757350424c436f757273653031" \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
--testnet-magic 1097911063
```


### 1d: Create Front End (lesson -> live-coding -> bounty)


## 2: `ValidateWithTokenInOutput.hs`

### This Contract Validates if some Auth token is being deposited at Contract address

### 2a: Compile and build Contract Address
- This is a parameterized validator: must define the Auth Token

```
cd /project-04/output
cardano-cli address build --payment-script-file validate-with-token-in-output.plutus --testnet-magic 1097911063 --out-file validate-with-token-in-output.addr
```

### 2b: Lock Funds
- In `validate-with-token-in-output.plutus`, we expect Datum and Redeemer to be integers, but it doesn't matter which. (see `/project-04/src/PpblEscrow/ValidateWithTokenInOutput.hs`)
- Just Hash a number, and note it:
```
cardano-cli transaction hash-script-data --script-data-value 1618
> 2da1c63e7646ce8cc514113c66e9cefb79e482210ad1dadb51c2a17ab14cf114
```

#### Set Variables
```
SENDER
SENDERKEY
CONTRACT="addr_test1wrj6t80qkhcnxp9rft464znd3vp68y0vs86ualf9f8juwts3xrtyw"
DATUMHASH="2da1c63e7646ce8cc514113c66e9cefb79e482210ad1dadb51c2a17ab14cf114"
TXIN=
```

```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TXIN \
--tx-in $TXIN2 \
--tx-out $CONTRACT+4000000 \
--tx-out-datum-hash $DATUMHASH \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file tx.raw \
--testnet-magic 1097911063

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file tx.raw \
--out-file tx.signed

cardano-cli transaction submit \
--tx-file tx.signed \
--testnet-magic 1097911063
```

### 2c: Unlock Funds
- Here, the TXOUT to the CONTRACT must include the specified auth token.
- `ValidateWithTokenInOutput.hs` assumes that the Auth token is "deposited" at the Contract address
- This is *closer* to what we want: we want to allow access to someone who holds an auth token; now the problem is that we can't really retreive those auth tokens from the contract address.


```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $SENDERTXIN \
--tx-in $CONTRACTTXIN \
--tx-in-script-file validate-with-token-in-output.plutus \
--tx-in-datum-value 1618 \
--tx-in-redeemer-value 0 \
--tx-in-collateral $COLLATERAL \
--tx-out $CONTRACT+"3500000 + 1 3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c.506c7574757350424c436f757273653031" \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
--testnet-magic 1097911063
```

## 3: `ValidateWithTokenInUserInput.hs`

### This Contract Validates if some Auth token is included in an input UTXO

### 3a: Compile and build Contract Address
- This is a parameterized validator: must define the Auth Token
- Expensive, but it works

```
cd /project-04/output
cardano-cli address build --payment-script-file validate-with-token-in-user-input.plutus --testnet-magic 1097911063 --out-file validate-with-token-in-user-input.addr
```

### 3b: Lock Funds
- In `validate-with-token-in-output.plutus`, we expect Datum and Redeemer to be integers, but it doesn't matter which. (see `/project-04/src/PpblEscrow/ValidateWithTokenInOutput.hs`)
- Just Hash a number, and note it:
```
cardano-cli transaction hash-script-data --script-data-value 1618
> 2da1c63e7646ce8cc514113c66e9cefb79e482210ad1dadb51c2a17ab14cf114
```

#### Set Variables
```
SENDER
SENDERKEY
CONTRACT="addr_test1wruf96sqstm2zlh9kzjurrucrxp3a4ljhtsuadxtchpm4fq6n9wxg"
DATUMHASH="2da1c63e7646ce8cc514113c66e9cefb79e482210ad1dadb51c2a17ab14cf114"
TXIN=
```

```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TXIN \
--tx-out $CONTRACT+2000000 \
--tx-out-datum-hash $DATUMHASH \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file tx.raw \
--testnet-magic 1097911063

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file tx.raw \
--out-file tx.signed

cardano-cli transaction submit \
--tx-file tx.signed \
--testnet-magic 1097911063
```

### 3c: Unlock Funds
- As long as one input UTXO includes the specified "auth token", the Tx will validate
- This is expensive. The transaction fee is ~.34 ADA -- but it works
- ALSO: this transaction build is lazy. It works, but by using `--change-address` to return funds to contract, we do not add datum to utxo...so don't actually do it this way

```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $SENDERTXIN \
--tx-in $CONTRACTTXIN \
--tx-in-script-file validate-with-token-in-user-input.plutus \
--tx-in-datum-value 1618 \
--tx-in-redeemer-value 0 \
--tx-in-collateral $COLLATERAL \
--tx-out $SENDER+2000000 \
--change-address $CONTRACT \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
--testnet-magic 1097911063
```