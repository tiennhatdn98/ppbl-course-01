{-# LANGUAGE TypeApplications #-}
{-#LANGUAGE DataKinds #-}
{-#LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module PpblEscrow.Bonfire.EscrowOffchain where 

import           Ledger.Constraints       as Constraints
import Plutus.Contract as Contract
import Data.Text (Text)
import           Control.Monad             (void, forever)
import qualified Data.Map                  as Map
import qualified PlutusTx
import PlutusTx.Prelude (BuiltinByteString)
import Ledger 
import PpblEscrow.Bonfire.EscrowContractDraft as Bonfire
import Prelude as P
import qualified Plutus.V1.Ledger.Ada as Ada
import              Ledger.Value        as Value
import Plutus.ChainIndex.Tx
import           Data.Aeson                (ToJSON, FromJSON)
import           GHC.Generics              (Generic)
import           Prelude                   (Show (..))
import           Schema                    (ToSchema)
import           Data.Void            (Void)

import PpblEscrow.Bonfire.Mint 
import qualified Ledger.Constraints as Contraints

data StartParams = StartParams 
                { sOrganizerReference :: !BuiltinByteString
                , sEventRefernce      :: !BuiltinByteString
                , sOrganizerPkh       :: !PubKeyHash 
                , sAttendeePkh        :: !PubKeyHash 
                , sEventCost          :: !Integer 
                , sEventStartTime     :: !POSIXTime -- how long?
                } deriving (Generic, ToJSON, FromJSON, ToSchema)


type EscrowSchema = Endpoint "start" StartParams
                            .\/ Endpoint "cancel" StartParams


start :: StartParams -> Contract w EscrowSchema Text ()
start params = do
    pkh <- Contract.ownPaymentPubKeyHash
    let cs = curSymbol pkh
        --datum 
        bonfireDat = BonfireEventEscrowDatum ("orgRef" <> sOrganizerReference params) ("eventRef" <> sEventRefernce params) (unPaymentPubKeyHash pkh) (sAttendeePkh params) (sEventCost params) (sEventStartTime params)
        --value we send to the contract 
        val = Ada.lovelaceValueOf 2000000 <> Value.singleton cs (TokenName ("event" <> sEventRefernce params)) 1
        tx      = Constraints.mustPayToTheScript bonfireDat val
    ledgerTx <- submitTxConstraints Bonfire.typedValidator tx
    void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
    Contract.logInfo @P.String "start transaction confirmed"

cancel :: StartParams -> Contract w EscrowSchema Text ()
cancel params = do
    let cs = curSymbol (PaymentPubKeyHash $ sOrganizerPkh params) 
        -- tn = event1
        tn = TokenName ("event" <> sEventRefernce params)
    --we have to find the tx we want to consume, returns (oref, ChaintdexTxOut, EscrowEventEscrowDatum)
    (oref, o, d) <- findUtxo cs (sOrganizerReference params)
    let val = Ada.lovelaceValueOf 2000000 <> Value.singleton cs tn 1
        -- redeemer used as action
        r = Redeemer $ PlutusTx.toBuiltinData Bonfire.Cancel
        lookups = Constraints.otherScript validator                 <>
                  Constraints.unspentOutputs (Map.singleton oref o)
        tx      = Contraints.mustSpendScriptOutput oref r <>
                  Constraints.mustPayToPubKey (PaymentPubKeyHash $ organizerAddress d) val <> 
                  mustValidateIn (from $ eventStartTime d)
    ledgerTx <- submitTxConstraintsWith @Void lookups tx
    void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
    Contract.logInfo @P.String "cancle transaction confirmed"

findUtxo :: CurrencySymbol -> BuiltinByteString -> Contract w s Text (TxOutRef, ChainIndexTxOut, BonfireEventEscrowDatum)
findUtxo cs ref = do
    -- get utxos at script address, retuns (Map TxOutRef (ChainIndexTxOut, ChainIndexTx))
    utxos <- utxosTxOutTxAt $ scriptAddress Bonfire.validator 
    let tn = TokenName "event1"
        -- we want the utxo which has the given token cs tn
        xs = [ (oref, (utxo, tx))
             | (oref, (utxo, tx)) <- Map.toList utxos
             , Value.valueOf (_ciTxOutValue utxo) cs tn == 1
             ]
    case xs of
        [(oref, (utxo, tx))] -> case txOutDatumHash $ toTxOut utxo of
            Nothing   -> throwError "unexpected out type"
            -- search for the datum, currently not needed because one token == one transaction (needed if same token for different txs)
            Just h -> case Map.lookup h $ _citxData tx of
                Nothing        -> throwError "datum not found"
                Just (Datum e) -> case PlutusTx.fromBuiltinData e of
                    Nothing -> throwError "datum has wrong type"
                    -- if datum then make it type BonfireEventEscrowDatum (recordWildCards)
                    Just d@BonfireEventEscrowDatum{..}
                        | eventReference == "eventRef" <> ref -> return (oref, utxo, d)
                        | otherwise                                           -> throwError "wrong refernce"
        _           -> throwError "utxo not found"


endpoints :: Contract () EscrowSchema Text ()
endpoints = forever
          $ handleError logError
          $ awaitPromise
          $ start' `select` cancel'
  where
    start'  = endpoint @"start" $ \params -> start params 
    cancel' = endpoint @"cancel" $ \params -> cancel params 
